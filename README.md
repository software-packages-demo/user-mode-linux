# user-mode-linux

Runs Linux as a user process under another Linux kernel. http://user-mode-linux.sourceforge.net/

## Official documentation
* [*UserModeLinux-HOWTO*](http://user-mode-linux.sourceforge.net/old/UserModeLinux-HOWTO.html)
* http://user-mode-linux.sourceforge.net
* [manpage(s)](https://manpages.debian.org/testing/user-mode-linux/)@debian.org

## Unofficial documentation
* [User-mode_Linux](https://en.m.wikipedia.org/wiki/User-mode_Linux)@Wikipedia
* [*User-mode Linux/Guide*](https://wiki.gentoo.org/wiki/User-mode_Linux/Guide)@wiki.gentoo.org
* [User-mode_Linux](https://wiki.archlinux.org/index.php/User-mode_Linux)@wiki.archlinux.org
* [UserModeLinux](https://wiki.debian.org/UserModeLinux)@wiki.debian.org
  * [*Running Linux on Linux - Virtual Servers*
  ](https://web.archive.org/web/20180304192833/https://debian-administration.org/article/48/Running_Linux_on_Linux_-_Virtual_Servers)
* [*Linux : User Mode Linux and Gentoo*](http://www.jjoseph.org/linux_work/user_mode_linux_and_gentoo) 2009-11
* [*Network lab with User Mode Linux*](https://vincent.bernat.ch/en/blog/2011-uml-network-lab) 2011-05 Vincent Bernat

### French unofficial documentation
* [*User Mode Linux ( UML )*](https://doc.ubuntu-fr.org/user_mode_linux_uml)@doc.ubuntu-fr.org
* [*Premiers pas avec User Mode Linux*](http://www.chicoree.fr/w/Premiers_pas_avec_User_Mode_Linux)
* [*Labo virtuel avec User Mode Linux*](https://vincent.bernat.ch/fr/blog/2011-lab-reseau-uml) 2011-05 Vincent Bernat

## Documentation for other software used in demo
### debootstrap
* [debootstrap(8)](https://manpages.debian.org/testing/debootstrap/debootstrap.8.en.html)
